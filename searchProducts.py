#!/usr/bin/python
from bs4 import BeautifulSoup
import requests
import csv

products = list(csv.reader(open("azeanCompleto.csv", "rb"), delimiter='\t'))
firstline = True
fo =  open("resultado.txt","wb")
for product in products:
	if firstline:
		firstline = False
		print("Generando archivo...")
		continue
	r = requests.get("http://busqueda.fnac.es/SearchResult/ResultList.aspx?&Search="+product[3])
	data = r.text
	soup = BeautifulSoup(data, 'lxml')
	resultado = soup.find("div", class_= "Search-title").find('h1').span.string
	fo.write("Producto: "+product[2]+"\n")
	fo.write("Part Number: "+product[0]+"\n")
	if int(resultado.strip()[1:-1]) > 0:
		for link in soup.find_all("p",class_= "Article-desc"):
			r2 = requests.get(link.a.get('href'))
			soup2 = BeautifulSoup(r2.text, 'lxml')
			resultado2 = soup2.find("div", class_= "js-ProductSellersTab-list")
			if resultado2 is None:
				cant_vendedores = 1
			else:
				cant_vendedores = resultado2.string.strip().split()[2]
			fo.write("- Marketplace presenta "+str(cant_vendedores)+" vendedores\n")
			precio = soup2.find("strong", class_="product-price").get_text().strip()
			fo.write("    Precio: "+str(precio.encode('utf8'))+"\n")
			envio = soup2.find("span", class_="DeliveryPrice")
			if envio is None:
				envio = soup2.find("p", class_="Delivery")
			envio = envio.get_text().strip()
			fo.write("    Envio: "+str(envio.encode('utf8'))+"\n")
			fo.write("	  URL: "+str(link.a.get('href'))+"\n")
	else:
		fo.write("No encontrado\n")
	fo.write("\n\n")
fo.close()